const Twit = require('twit');
const mongoose = require('mongoose');
const config = require('../config');

var Schema = mongoose.Schema;

//Create a mongoose Schema in order to put each tweet
const tweetSchema = new Schema({ any: Schema.Types.Mixed }, { strict: false });
const TweetModel = mongoose.model('tweets', tweetSchema);

//Create an array with the accounts of the leaders
const accounts = [
    '@atsipras',
    '@St_Theodorakis',
    '@kmitsotakis',
    '@FofiGennimata',
    '@yanisvaroufakis',
    '@PanosKammenos',
];

var T = new Twit(config.twitter);

/* Save in mongodb the tweets of the specific accounts */
var insertTweet = function (result) {
        var tweet_object = new TweetModel(result);
        tweet_object.save();
}

const Tweet = function () {

    this.getPrimeMinistersTweets = async function (res , count) {
        var data = [];
        for (var account of accounts) {

            let promise = new Promise((resolve, reject) => {
                this.getAccount(account, count, resolve, reject);
            })
            .then(function(result) {
                for (const twit of result) {
                    data.push(twit);
                    insertTweet(twit);
                }
            })

            await promise;

        }

        if (!res) {
            console.log('Data saved successfully');
        } else if (res) {
            res.send(data);
        }
    }

    this.getAccount = function (screen_name, count, resolve, reject) {

        if (screen_name) {

            var options = { screen_name , count };

            T.get('statuses/user_timeline', options , function(err, data) {
                if (err) {
                    console.log(err.message);
                    reject();
                } else {
                    resolve(data);
                }
            })
        }
    }
}



module.exports = Tweet;
