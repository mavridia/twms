# Project Title

Greek Twitter Political Graph Exploration


[Course Object - Course Project 2019](https://eclass.teicrete.gr/modules/document/?course=TP261)

## Getting Started

Team 1
In order to collect all politics’ tweets we designed this part of the project, which uses the Twitter API to collect all the useful information.


### Prerequisites

For the implementation of the backend part of the project we used Express.js framework and MongoDB as in the end we needed to create a CSV file produced from the JSON that MongoDB  uses.

### Installing
First each user should download NodeJs, then MongoDb and create a database named twits in the Mongo shell (every useful command is in MOngo documentation).
In the package.json file, there are all the dependencies each user must use to run this project.The only command must be run in console is the following.

*******  npm install  ******

## Running 
First each user should get license from Twitter team to generate all keys and access tokens to have access to Twitter API, then the keys must be filled in the config.js file.

Next step to run use the following command:   

*******  npm run devstart ******

## Built With

* [NodeJs](https://nodejs.org/en/)  - NodeJs
* [Express](https://expressjs.com/) - The web framework used
* [Mongo](https://www.mongodb.com/) - Database
* [npm](https://www.npmjs.com/) 


## Authors

* **Zervoudakis Stefanos** - *Contact* - [stezer95@gmail.com](https://gmail.com/)

* **Mavridi Anastasia**    - *Contact* -[mavridia@gmail.com](https://gmail.com/)


## License
This project is licensed under the Technological Educational Institute of Crete  ([TEI](https://www.teicrete.gr/en))

