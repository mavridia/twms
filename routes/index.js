var express = require('express');
var router = express.Router();
const Tweet = require('../models/Tweet');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Tweets' });
});

router.get('/getTweets', function(req, res, next) {

  let tweet = new Tweet();
//Get the last 3 tweets, if we want more tweets we should change only the parameter of the following function
  tweet.getPrimeMinistersTweets(res, 3);

});

module.exports = router;
